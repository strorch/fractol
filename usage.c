/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   usage.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstorcha <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/26 14:53:03 by mstorcha          #+#    #+#             */
/*   Updated: 2018/03/26 14:53:07 by mstorcha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	print_usage(void)
{
	ft_putstr("Usage: ./fractol <fractal number>\n");
	ft_putendl("Draws the specified fractal.");
	ft_putendl("Try '--help' for more information.");
	exit(1);
}

void	print_help(void)
{
	ft_putendl("List of fractals:");
	ft_putendl("1 - Mandelbrot set");
	ft_putendl("2 - Julia set");
	ft_putendl("3 - Burning ship set");
	ft_putendl("4 - Mandelblar set");
	ft_putendl("5 - Celtic Mandelbrot set");
	ft_putendl("6 - Buffalo set");
	exit(1);
}

void	print_wn(char *num)
{
	ft_putstr("Error: ");
	ft_putstr(num);
	ft_putendl(" - wrong input!");
	ft_putendl("Try '--help' for more information!");
	exit(1);
}
