/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_debug.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstorcha <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/27 18:44:06 by mstorcha          #+#    #+#             */
/*   Updated: 2018/03/27 18:44:11 by mstorcha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static char	*ft_dtoa(double d, int p, char *delimeter)
{
	char *str1;
	char *str2;
	char *res;
	char *buf;
	char sign[2];

	sign[0] = ' ';
	sign[1] = '\0';
	if (d < 0)
	{
		sign[0] = '-';
		d = -d;
	}
	str1 = ft_itoa((int)d);
	buf = ft_strjoin(str1, delimeter);
	str2 = ft_itoa((int)((d - (int)d) * 10 * p));
	res = ft_strjoin(buf, str2);
	free(str1);
	str1 = res;
	res = ft_strjoin(sign, res);
	ft_strdel(&str1);
	ft_strdel(&buf);
	ft_strdel(&str2);
	return (res);
}

static void	help_string_draw(int location,
		char *str1, double to_itoa, t_main main)
{
	char *str;
	char *itoated;

	itoated = ft_dtoa(to_itoa, 6, ".");
	str = ft_strjoin(str1, itoated);
	mlx_string_put(main.mlx_ptr, main.win_ptr, 5, location, 0x00FF00, str);
	free(itoated);
	free(str);
}

void		draw_debug(t_main main, void *mlx_ptr, void *win_ptr)
{
	mlx_string_put(mlx_ptr, win_ptr, 5, 5, 0x00FF00, "Fract'ol info:");
	mlx_string_put(mlx_ptr, win_ptr, 5, 25, 0x00FF00, "Author - mstorcha");
	help_string_draw(45, "IMAGINE = ", (double)main.part.c_im, main);
	help_string_draw(65, "REAL = ", (double)main.part.c_re, main);
	help_string_draw(85, "NUM ITER = ", main.iter, main);
}
