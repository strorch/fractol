/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstorcha <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/13 21:00:18 by mstorcha          #+#    #+#             */
/*   Updated: 2018/03/13 21:00:19 by mstorcha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H

# define HEIGHT 1000
# define WIDTH 800

# include "libft/libft.h"
# include "minilibx_macos/mlx.h"
# include <math.h>
# include <pthread.h>

typedef struct	s_image
{
	void		*data;
	int			*ptr;
	int			bits;
	int			sz_l;
	int			endi;
}				t_image;

typedef struct	s_part
{
	float		c_re;
	float		c_im;
}				t_part;

typedef struct	s_main
{
	char		*f_type;
	void		*mlx_ptr;
	void		*win_ptr;
	float		move_x;
	float		move_y;
	float		zoom;
	int			iter;
	int			pp;
	int			rr;
	t_part		part;
	t_image		image;
	int			mv_mode;
}				t_main;

void			draw_buffalo(t_main *mlx, void *mlx_ptr, void *win_ptr);
void			draw_celtic_mandelbrot(t_main *mlx,
						void *mlx_ptr, void *win_ptr);
void			init_fract(t_main *mlx, char *argv);
void			transform_and_draw(t_main mlx);
int				ft_mouse_move_handler(int x, int y, t_main *mlx);
float			lerp(float v0, float v1, float t);
int				mouse(int key_code, int x, int y, t_main *mlx);
int				key(int key_code, t_main *mlx);
void			draw_debug(t_main main, void *mlx_ptr, void *win_ptr);
void			draw_mandelbrot(t_main *mlx, void *mlx_ptr, void *win_ptr);
void			draw_burn_ship(t_main *mlx, void *mlx_ptr, void *win_ptr);
void			draw_mandelbar(t_main *mlx, void *mlx_ptr, void *win_ptr);
void			draw_julia(t_main mlx, void *mlx_ptr, void *win_ptr);
unsigned int	get_color(int i);
void			print_usage(void);
void			print_help(void);
void			print_wn(char *num);

#endif
