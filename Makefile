# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mstorcha <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/01/26 15:48:11 by mstorcha          #+#    #+#              #
#    Updated: 2018/03/02 20:59:40 by mstorcha         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = fractol
LIBA = libft/libft.a
MLXA = minilibx_macos/libmlx.a

CC = gcc

CFLAGS = -Wall -Wextra -Werror

LIB_DIR = ./libft/
LIB_FLAGS = -L$(LIB_DIR) -lft

MLX_DIR = ./minilibx_macos/
LIB_FLAGS += -L$(MLX_DIR) -lmlx -framework OpenGL -framework AppKit

SOURCES = main.c \
			mandelbrot_set.c \
			julia_set.c \
			colors.c \
			usage.c \
			print_debug.c \
			burn_ship_set.c \
			mandelbar_set.c \
			heandlers.c \
			celtic_set.c \
			buffalo_set.c

OPTION = -I.

OBJ = $(SOURCES:.c=.o)

all: $(NAME)

$(NAME): $(LIBA) $(MLXA) $(OBJ)
	$(CC) $(OBJ) $(LIB_FLAGS) -o $(NAME)

$(LIBA): lib

$(MLXA): libmlx

lib:
	@make all -C $(LIB_DIR)

libmlx:
	@make all -C $(MLX_DIR)

clean:
	@make clean -C $(LIB_DIR)
	@/bin/rm -f $(OBJ)

fclean: clean
	@make clean -C $(MLX_DIR)
	@make fclean -C $(LIB_DIR)
	@/bin/rm -f $(NAME)

re: fclean all