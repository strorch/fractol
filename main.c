/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstorcha <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/28 16:51:27 by mstorcha          #+#    #+#             */
/*   Updated: 2018/02/28 19:06:46 by mstorcha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	transform_and_draw(t_main mlx)
{
	int num;

	num = ft_atoi(mlx.f_type);
	mlx_clear_window(mlx.mlx_ptr, mlx.win_ptr);
	if (num == 1)
		draw_mandelbrot(&mlx, mlx.mlx_ptr, mlx.win_ptr);
	else if (num == 2)
		draw_julia(mlx, mlx.mlx_ptr, mlx.win_ptr);
	else if (num == 3)
		draw_burn_ship(&mlx, mlx.mlx_ptr, mlx.win_ptr);
	else if (num == 4)
		draw_mandelbar(&mlx, mlx.mlx_ptr, mlx.win_ptr);
	else if (num == 5)
		draw_celtic_mandelbrot(&mlx, mlx.mlx_ptr, mlx.win_ptr);
	else if (num == 6)
		draw_buffalo(&mlx, mlx.mlx_ptr, mlx.win_ptr);
	else
		print_wn(mlx.f_type);
	draw_debug(mlx, mlx.mlx_ptr, mlx.win_ptr);
}

void	init_fract(t_main *mlx, char *argv)
{
	mlx->f_type = argv;
	mlx->zoom = 0.5;
	mlx->move_x = 0;
	mlx->move_y = 0;
	mlx->iter = 20;
	mlx->pp = 2;
	mlx->rr = 1;
	mlx->part.c_im = 0.27015;
	mlx->part.c_re = -0.7;
	mlx->mv_mode = 1;
}

int		main(int argc, char **argv)
{
	t_main mlx;

	if (argc != 2)
		print_usage();
	if (!ft_strcmp("--help", argv[1]))
		print_help();
	mlx.mlx_ptr = mlx_init();
	mlx.win_ptr = mlx_new_window(mlx.mlx_ptr, HEIGHT, WIDTH, "Fract'ol");
	mlx.image.data = mlx_new_image(mlx.mlx_ptr, HEIGHT, WIDTH);
	mlx.image.ptr = (int *)mlx_get_data_addr(mlx.image.data,
		&mlx.image.bits, &mlx.image.sz_l, &mlx.image.endi);
	init_fract(&mlx, argv[1]);
	transform_and_draw(mlx);
	mlx_hook(mlx.win_ptr, 2, 0, &key, &mlx);
	if (ft_atoi(argv[1]) == 2)
		mlx_hook(mlx.win_ptr, 6, 0, &ft_mouse_move_handler, &mlx);
	mlx_mouse_hook(mlx.win_ptr, &mouse, &mlx);
	mlx_hook(mlx.win_ptr, 17, 0, (int (*)())exit, NULL);
	mlx_loop(mlx.mlx_ptr);
	return (0);
}
