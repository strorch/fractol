/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   buffalo_set.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstorcha <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/29 13:35:11 by mstorcha          #+#    #+#             */
/*   Updated: 2018/03/29 13:35:17 by mstorcha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static int	return_i(t_main mlx)
{
	int		i;
	double	new_re;
	double	new_im;
	double	old_re;
	double	old_im;

	new_re = 0;
	new_im = 0;
	old_re = 0;
	old_im = 0;
	i = -1;
	while (++i < mlx.iter)
	{
		old_re = new_re;
		old_im = new_im;
		new_re = fabs(old_re * old_re - old_im * old_im) + mlx.part.c_re;
		new_im = 2 * fabs(old_re * old_im) + mlx.part.c_im;
		if ((new_re * new_re + new_im * new_im) > 4)
			break ;
	}
	return (i);
}

void		draw_buffalo(t_main *mlx, void *mlx_ptr, void *win_ptr)
{
	int x;
	int y;

	y = -1;
	while (++y < WIDTH)
	{
		x = -1;
		while (++x < HEIGHT)
		{
			mlx->part.c_re = 1.5 * ((double)x /
				1.5 - WIDTH / 2) / (0.5 * mlx->zoom * WIDTH) + mlx->move_x;
			mlx->part.c_im = (y - HEIGHT / 2) /
				(0.5 * mlx->zoom * HEIGHT) + mlx->move_y;
			if (return_i(*mlx) != mlx->iter)
				mlx->image.ptr[y * HEIGHT + x] =
					get_color(return_i(*mlx));
			else
				mlx->image.ptr[y * HEIGHT + x] = 0;
		}
	}
	mlx_put_image_to_window(mlx_ptr, win_ptr, mlx->image.data, 0, 0);
}
