/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   heandlers.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstorcha <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/28 20:05:40 by mstorcha          #+#    #+#             */
/*   Updated: 2018/03/28 20:05:42 by mstorcha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		ft_mouse_move_handler(int x, int y, t_main *mlx)
{
	int e;

	if (mlx->mv_mode == 1)
	{
		e = 0;
		(++e) ? (mlx->part.c_re = (-1.4 * x) / HEIGHT) : 0;
		(++e) ? (mlx->part.c_im = 0.5403 * y / WIDTH) : 0;
		if (e)
			transform_and_draw(*mlx);
	}
	return (0);
}

float	lerp(float v0, float v1, float t)
{
	return (1.0 - t) * v0 + t * v1;
}

int		mouse(int key_code, int x, int y, t_main *mlx)
{
	int e;

	e = 0;
	(key_code == 4 && ++e) ? (mlx->zoom /= pow(1.001, 60)) : (0);
	if (key_code == 5 && ++e)
	{
		mlx->move_x = lerp(mlx->move_x
			, 1.5 * (x - HEIGHT / 2) / (0.5 * mlx->zoom * HEIGHT) + mlx->move_x
			, 0.06);
		mlx->move_y = lerp(mlx->move_y
			, 1.5 * (y - WIDTH / 2) / (0.5 * mlx->zoom * WIDTH) + mlx->move_y
			, 0.06);
		mlx->zoom *= pow(1.001, 60);
	}
	if (e)
		transform_and_draw(*mlx);
	return (0);
}

int		key(int key_code, t_main *mlx)
{
	char	e;

	e = 0;
	(key_code == 53) ? exit(1) : 0;
	(key_code == 124 && ++e) ? (mlx->move_x += 0.058 / mlx->zoom) : 0;
	(key_code == 123 && ++e) ? (mlx->move_x -= 0.058 / mlx->zoom) : 0;
	(key_code == 126 && ++e) ? (mlx->move_y -= 0.058 / mlx->zoom) : 0;
	(key_code == 125 && ++e) ? (mlx->move_y += 0.058 / mlx->zoom) : 0;
	(key_code == 24 && ++e) ? (mlx->iter += 1) : 0;
	(key_code == 27 && mlx->iter > 0 && ++e) ? (mlx->iter -= 1) : 0;
	(key_code == 31 && ++e) ? (mlx->mv_mode = 1) : 0;
	(key_code == 35 && ++e) ? (mlx->mv_mode = 0) : 0;
	(key_code == 49 && ++e) ? (init_fract(mlx, mlx->f_type)) : 0;
	e ? transform_and_draw(*mlx) : 0;
	return (0);
}
